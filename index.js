// Exponent operator


const firstNum = 8**2;
console.log(firstNum);


// EXponent for ES6 Updates 
const secondNum = Math.pow(8,2);
console.log(secondNum);

// Template literala

let name ="John";

// pre-template literal string
let message = 'hello ' + name + ' ! Welcome to programing';
console.log("mesage without template tempalte literal")

// Using Template literal (backticks - ``)
message = `Hello ${name} ! welcome to programming`;
console.log(`Message with tempalate literal: ${message}`);



// scape on pre-tempalate
const anotherMessage= `
${name} attended a "math competetion".
He won it by solving the problem 8 **2 
with the solution of ${secondNum}`;

console.log(anotherMessage);


// computing interest
const interestRate = 0.1;
const principal = 1000;

console.log(`The interest of your savings accout is ${principal * interestRate}`)


// Array and object Destructuring

// Array destructuring- allows to unpack element in arrays into distinct values

const fullName = ["juan", "Dela", "Cruz"];

// pre aary dustructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello  ${fullName[0]}, ${fullName[1]}, ${fullName[2]} Its nice to meet you`)


// array destucturing-----------
const [firstName, middleName, lastName]= fullName;
console.log(`Hello  ${firstName}, ${middleName}, ${lastName} Its nice to meet you`)


// Object Destructuring - allows us to unpack properties of object into distinct

const person = {
	givenName: "Jane",
	maidenNamee: " Smith",
	familyName: "Rogers"
}

// Pre objecr destructure
console.log(person.givenName);
console.log(person.maidenNamee);
console.log(person.familyName);


// Object Destructuring
const {givenName,maidenNamee,familyName} = person;
console.log(givenName);
console.log(maidenNamee);
console.log(familyName)

function getFullName({givenName,maidenNamee,familyName}               ){
	console.log(`${givenName} ${maidenNamee} ${familyName}`)
}
getFullName(person);


/*Arrow functions- comapct alternative syntax to traditional functions
	-conventional function
	function	nameOfTheFuncion(){
	statement;
}

arrow functions syntax:
	conts variable = () =>{
	console.log();
	}

*/

/*
function printFullName(firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}
printFullName("John", "D." "Smith");
*/


const printFullName = (firstName, middleInitial, lastName) =>{
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("John", "D.", "Smith");


//  literal for each function-----------------------------

const students = ["John", "jane", "Judy"]
console.log(`Pre arrow function`);
students.forEach(function(student){   
	console.log(`${student} is a student.`);
})


// for each arrow
 /// tinanggal si function to covert it to arrow function
console.log(`Arrow Function`);
students.forEach(student => {
	console.log(`${student} is a student.`)
})




/*
const add = (x,y) => {
	return x+y;
}
*/
const add = ( x,y) => x+y;
console.log(add(5,8));


let total = add(1,6);
console.log(total);



// Default function argument value

const greet = (name = 'User') =>{
	return `Goodmorning, ${name}`;
}
console.log(greet());


const pokemon = (name = 'Pokemon') =>{
	return `I got you, ${name}`;
}
console.log(pokemon());
console.log(pokemon("Mama"));/// over write the default


// class- based object blueprint - allow creation/instatiation of object sing classes as blue prints

/*syntax:
	class className{
	constructor(ObjectPropertyA, objectpropertyB){
	this.objectPropertyA = objectPropertyA
	this.objectPropertyB = objectPropertyB
	
	}
	}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
		
	}
}
const myCar = new Car();
console.log(myCar);
myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = "2021"

console.log(myCar)


//  shortut term for adding values
const myNewCar = new Car("Toyota", "vios", 2021);
console.log(myNewCar);